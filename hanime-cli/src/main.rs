use anyhow::bail;
use anyhow::Context;
use std::path::PathBuf;

#[derive(argh::FromArgs)]
#[argh(description = "A tool to download videos from hanime")]
struct CommandOptions {
    #[argh(subcommand)]
    subcommand: Subcommand,
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(subcommand)]
enum Subcommand {
    Search(SearchOptions),
    Download(DownloadOptions),
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "search",
    description = "search for a video on hanime"
)]
pub struct SearchOptions {
    /// the search query
    #[argh(positional)]
    query: String,
}

#[derive(argh::FromArgs, PartialEq, Debug)]
#[argh(
    subcommand,
    name = "download",
    description = "download videos from hanime"
)]
pub struct DownloadOptions {
    /// the video url
    #[argh(positional)]
    url: String,

    /// the dir to save the video
    #[argh(option)]
    out_dir: Option<PathBuf>,
}

fn main() -> anyhow::Result<()> {
    let options: CommandOptions = argh::from_env();

    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to create tokio runtime")?;
    tokio_rt.block_on(async_main(options))
}

async fn async_main(options: CommandOptions) -> anyhow::Result<()> {
    let client = hanime::Client::new();

    match options.subcommand {
        Subcommand::Search(options) => exec_search(client, options).await,
        Subcommand::Download(options) => exec_download(client, options).await,
    }
}

async fn exec_search(client: hanime::Client, options: SearchOptions) -> anyhow::Result<()> {
    let results = client
        .search(&options.query)
        .await
        .with_context(|| format!("failed to search for '{}'", options.query))?;
    let hits = results.get_hits().context("failed to parse search hits")?;

    println!("Search Results");
    for (i, hit) in hits.iter().enumerate() {
        println!("{}) {} [{}]", i + 1, hit.name, hit.get_url().as_str());
    }

    if hits.is_empty() {
        println!("No results.");
    }

    Ok(())
}

async fn exec_download(client: hanime::Client, options: DownloadOptions) -> anyhow::Result<()> {
    let url = match hanime::Url::parse(&options.url) {
        Ok(url) => url,
        Err(error) => {
            eprintln!("Input is not a valid url: {error}");
            eprintln!("Assuming its a slug...");
            hanime::video_url_from_slug(&options.url)
        }
    };

    eprintln!("Fetching '{}'...", url.as_str());

    let info = client
        .get_hentai_video_info(url.as_str())
        .await
        .context("failed to get hentai video info")?;
    let best_stream = info
        .get_best_accessible_video_stream()
        .context("no accessible video streams found")?;

    eprintln!("Downloading \"{}\"...", best_stream.url);

    let mut filename = PathBuf::from(format!("{}.mp4", info.get_slug()));
    if let Some(out_dir) = options.out_dir {
        filename = out_dir.join(filename);
    }

    if tokio::fs::try_exists(&filename).await? {
        eprintln!("file \"{}\" exists", filename.display());

        return Ok(());
    }

    let ffmpeg_status = tokio::process::Command::new("ffmpeg")
        .args(["-i", &best_stream.url, "-acodec", "copy", "-vcodec", "copy"])
        .arg(&filename)
        .status()
        .await
        .context("failed to launch FFMpeg")?;

    if !ffmpeg_status.success() {
        bail!("FFmpeg failed with status \"{ffmpeg_status}\"");
    }

    Ok(())
}
