# hanime-rs 

[![Build status](https://ci.appveyor.com/api/projects/status/u4i5cvjrg09v0f9g?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/hanime-rs)
[![](https://tokei.rs/b1/bitbucket/professionalProgrammers/hanime-rs)](https://bitbucket.org/professionalProgrammers/hanime-rs)

A Rust API for https://hanime.tv.

