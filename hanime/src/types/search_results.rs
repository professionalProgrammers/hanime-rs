use std::collections::HashMap;
use url::Url;

#[derive(serde::Deserialize, Debug)]
pub struct SearchResults {
    /// Search Hits Json
    pub hits: String,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl SearchResults {
    /// Get the search hits
    pub fn get_hits(&self) -> Result<Vec<SearchHit>, serde_json::Error> {
        serde_json::from_str(&self.hits)
    }
}

/// A search hit
#[derive(serde::Deserialize, Debug)]
pub struct SearchHit {
    /// Hit Description
    pub description: String,

    /// Hit titles
    pub titles: Vec<String>,

    /// Likes
    pub likes: u64,

    /// Views
    pub views: u64,

    /// Dislikes
    pub dislikes: u64,

    /// Tags
    pub tags: Vec<String>,

    /// Name
    pub name: String,

    /// Url Slug
    pub slug: String,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl SearchHit {
    /// Get the url for this [`SearchHit`].
    pub fn get_url(&self) -> Url {
        crate::video_url_from_slug(&self.slug)
    }
}
