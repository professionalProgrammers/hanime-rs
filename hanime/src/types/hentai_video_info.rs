use std::collections::HashMap;
use std::time::Duration;
use url::Url;

/// Hentai Video Info
pub type HentaiVideoInfo = crate::types::NuxtData<Data>;

impl HentaiVideoInfo {
    /// Get the best accessible video stream.
    ///
    /// "Best" is currently defined as max(width * height) or filesize_mb if the dimensions are equal.
    pub fn get_best_accessible_video_stream(&self) -> Option<&Stream> {
        self.state
            .data
            .video
            .videos_manifest
            .servers
            .iter()
            .flat_map(|server| server.streams.iter())
            .filter(|stream| !stream.url.is_empty())
            .max_by_key(|stream| (stream.width * stream.height, stream.filesize_mbs))
    }

    /// Get the url slug
    #[inline]
    pub fn get_slug(&self) -> &str {
        &self.state.data.video.hentai_video.slug
    }

    /// Get the hentai video
    #[inline]
    pub fn get_hentai_video(&self) -> &HentaiVideo {
        &self.state.data.video.hentai_video
    }

    /// Get the hentai franchise
    #[inline]
    pub fn get_hentai_franchise(&self) -> &HentaiFranchise {
        &self.state.data.video.hentai_franchise
    }
}

/// The NUXT data
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Data {
    /// The video field
    pub video: Video,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

/// The video field of data
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Video {
    /// Base for videos?
    pub player_base_url: Url,

    /// Hentai Video Info
    pub hentai_video: HentaiVideo,

    /// Hentai Tags
    pub hentai_tags: Box<[HentaiTag]>,

    /// The hentai franchise
    pub hentai_franchise: HentaiFranchise,

    /// Hentai franchise videos
    pub hentai_franchise_hentai_videos: Box<[HentaiVideo]>,

    /// The next hentai video
    pub next_hentai_video: HentaiVideo,

    /// Video Manifest
    pub videos_manifest: VideosManifest,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct HentaiVideo {
    /// Video id
    pub id: u64,

    /// ?
    ///
    /// This is not present for hentai franchise video metadata
    pub is_visible: Option<bool>,

    /// Video Name
    pub name: Box<str>,

    /// URL slug
    pub slug: Box<str>,

    /// Created date
    pub created_at: Box<str>,

    /// Release date
    pub released_at: Box<str>,

    /// HTML Description
    ///
    /// This is not present for hentai franchise video metadata
    pub description: Option<Box<str>>,

    /// Views
    pub views: u64,

    /// Interests?
    pub interests: u64,

    /// Poster url
    pub poster_url: Url,

    /// Cover Url
    pub cover_url: Url,

    /// Is hard subtitled
    pub is_hard_subtitled: bool,

    /// Brand?
    pub brand: Box<str>,

    /// The duration in milliseconds
    #[serde(
        deserialize_with = "deserialize_millis_to_duration",
        serialize_with = "serialize_duration_to_millis"
    )]
    pub duration_in_ms: Duration,

    /// Whether this is censored
    pub is_censored: bool,

    /// Tags
    ///
    /// This is not present for hentai franchise video metadata
    pub hentai_tags: Option<Vec<serde_json::Value>>,

    /// Titles
    ///
    /// This is not present for hentai franchise video metadata
    pub titles: Option<Vec<serde_json::Value>>,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

/// A Hentai Tag
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct HentaiTag {
    /// ID
    pub id: u64,

    /// The tag name
    pub text: Box<str>,

    /// The number of videos with this tag
    pub count: u64,

    /// The tag description
    pub description: Box<str>,

    /// ?
    pub wide_image_url: Url,

    /// ?
    pub tall_image_url: Url,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

/// A hentai franchise
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct HentaiFranchise {
    /// ID
    pub id: u64,

    /// The name
    pub name: Box<str>,

    /// The slug
    pub slug: Box<str>,

    /// The title
    pub title: Box<str>,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct VideosManifest {
    /// Servers
    pub servers: Box<[Server]>,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

/// A server
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Server {
    /// Server streams
    pub streams: Box<[Stream]>,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Stream {
    /// Stream url. This is sometimes empty.
    pub url: Box<str>,

    /// Whether this is accesible for guests
    pub is_guest_allowed: bool,

    /// Video Size
    pub filesize_mbs: u64,

    /// Stream Kind
    pub kind: Box<str>,

    /// Stream width
    pub width: u64,

    /// Stream height.
    ///
    /// This is a string for some reason, but is parsed to an integer and serialized back into a string.
    #[serde(
        deserialize_with = "deserialize_str_to_u64",
        serialize_with = "serialize_u64_to_str"
    )]
    pub height: u64,

    /// The duration in milliseconds
    #[serde(
        deserialize_with = "deserialize_millis_to_duration",
        serialize_with = "serialize_duration_to_millis"
    )]
    pub duration_in_ms: Duration,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<Box<str>, serde_json::Value>,
}

impl Stream {
    /// Whether this stream is hls.
    pub fn is_hls(&self) -> bool {
        &*self.kind == "hls"
    }
}

/// Deserialize a str to a u64
fn deserialize_str_to_u64<'de, D>(deserializer: D) -> Result<u64, D::Error>
where
    D: serde::Deserializer<'de>,
{
    struct StrToU64Visitor;

    impl<'de> serde::de::Visitor<'de> for StrToU64Visitor {
        type Value = u64;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            write!(formatter, "a numeric string")
        }

        fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            s.parse::<u64>()
                .map_err(|_e| E::invalid_value(serde::de::Unexpected::Str(s), &"a numeric string"))
        }

        fn visit_string<E>(self, s: String) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            self.visit_str(&s)
        }
    }

    deserializer.deserialize_str(StrToU64Visitor)
}

/// Serialize a u64 to a str
fn serialize_u64_to_str<S>(v: &u64, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.collect_str(v)
}

/// Deserialize milliseconds (an integer) to a Duration
fn deserialize_millis_to_duration<'de, D>(deserializer: D) -> Result<Duration, D::Error>
where
    D: serde::Deserializer<'de>,
{
    use serde::Deserialize;

    let millis = u64::deserialize(deserializer)?;
    Ok(Duration::from_millis(millis))
}

/// Serialize a duration to milliseconds (an integer)
fn serialize_duration_to_millis<S>(value: &Duration, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    use serde::ser::Error;

    let value = u64::try_from(value.as_millis())
        .map_err(|_e| S::Error::custom("milliseconds cannot fit in a u64"))?;
    serializer.serialize_u64(value)
}

#[cfg(test)]
mod test {
    use super::*;

    const HENTAI_VIDEO_INFO: &str = include_str!("../../test_data/hentai_video_info.json");

    #[test]
    fn parse() {
        let _info: HentaiVideoInfo =
            serde_json::from_str(HENTAI_VIDEO_INFO).expect("failed to parse");
    }
}
