use std::collections::HashMap;

/// NUXT data
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct NuxtData<T> {
    /// The state
    pub state: State<T>,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// The state field
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct State<T> {
    /// The data
    pub data: T,

    /// Unknown k/vs
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
