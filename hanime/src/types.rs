/// [`HentaiVideoInfo`] and friends.
pub mod hentai_video_info;
/// The [`NuxtData`] type.
pub mod nuxt_data;
/// [`SearchResults`] and friends.
pub mod search_results;

pub use self::hentai_video_info::HentaiFranchise;
pub use self::hentai_video_info::HentaiVideo;
pub use self::hentai_video_info::HentaiVideoInfo;
pub use self::nuxt_data::NuxtData;
pub use self::search_results::SearchResults;

/// A search request
#[derive(Debug, serde::Serialize)]
pub struct SearchRequest<'a> {
    /// ?
    pub blacklist: Vec<()>,

    /// ?
    pub brands: Vec<()>,

    /// What to order by
    ///
    /// Options:
    /// * `created_at_unix`
    pub order_by: &'static str,

    /// How to order results
    ///
    /// Options:
    /// * `desc`
    pub ordering: &'static str,

    /// The page number.
    ///
    /// Starts at 0.
    pub page: u32,

    /// the search
    pub search_text: &'a str,

    /// ?
    pub tags: Vec<()>,

    /// ?
    ///
    /// Options:
    /// * `AND`
    pub tags_mode: &'static str,
}
