/// [`Client`]
pub mod client;
/// Api Types for this lib
pub mod types;

pub use crate::client::Client;
pub use crate::types::HentaiFranchise;
pub use crate::types::HentaiVideo;
pub use crate::types::HentaiVideoInfo;
pub use crate::types::SearchResults;
pub use url::Url;

/// Library Error Type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Reqwest HTTP Error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    /// Missing NUXT Data
    #[error("missing NUXT data")]
    MissingNuxtData,

    /// A Tokio task failed to join
    #[error(transparent)]
    TokioJoin(#[from] tokio::task::JoinError),

    /// Json Error
    #[error(transparent)]
    Json(#[from] serde_json::Error),

    /// Not Found Error
    #[error("the page was not found")]
    NotFound,
}

impl Error {
    /// Returns `true` if this error is the `NotFound` variant
    pub fn is_not_found(&self) -> bool {
        matches!(self, Error::NotFound)
    }
}

/// Make a video url from a url slug.
pub fn video_url_from_slug(slug: &str) -> Url {
    let mut base = Url::parse("https://hanime.tv/videos/hentai").expect("invalid url base");
    base.path_segments_mut()
        .expect("invalid url base")
        .push(slug);
    base
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn it_works() {
        let client = Client::new();
        let search = client.search("overflow").await.expect("failed to search");
        let hits = search.get_hits().expect("failed to get hits");
        dbg!(hits);

        let hentai_url = "https://hanime.tv/videos/hentai/overflow-season-1";
        let info = client
            .get_hentai_video_info(hentai_url)
            .await
            .expect("failed to get hentai video info");
        dbg!(info.get_best_accessible_video_stream());
    }

    #[tokio::test]
    async fn not_found() {
        let client = Client::new();

        let hentai_url = "https://hanime.tv/videos/hentai/test";
        let error = client.get_hentai_video_info(hentai_url).await.unwrap_err();
        assert!(error.is_not_found());
    }
}
