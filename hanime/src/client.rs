use crate::types::NuxtData;
use crate::types::SearchRequest;
use crate::Error;
use crate::HentaiVideoInfo;
use crate::SearchResults;
use once_cell::sync::Lazy;
use regex::Regex;
use serde::de::DeserializeOwned;

/// A HAnime Client
#[derive(Debug, Clone)]
pub struct Client {
    /// The inner HTTP client.
    ///
    /// You shouldn't use this directly.
    pub client: reqwest::Client,
}

impl Client {
    /// Make a new [`Client`].
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::new(),
        }
    }

    /// Search for a hentai video
    pub async fn search(&self, search_text: &str) -> Result<SearchResults, Error> {
        Ok(self
            .client
            .post("https://search.htv-services.com/")
            .json(&SearchRequest {
                blacklist: vec![],
                brands: vec![],
                order_by: "created_at_unix",
                ordering: "desc",
                page: 0,
                search_text,
                tags: vec![],
                tags_mode: "AND",
            })
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Get a url and extract NUXT data from it
    async fn get_nuxt<T>(&self, url: &str) -> Result<NuxtData<T>, Error>
    where
        T: DeserializeOwned + Send + 'static,
    {
        static REGEX: Lazy<Regex> = Lazy::new(|| {
            Regex::new(r#"window\.__NUXT__=(.*);"#).expect("invalid `get_nuxt` regex")
        });

        let response = self.client.get(url).send().await?.error_for_status()?;

        // The website does not properly 404
        if response.url().as_str() == "https://hanime.tv/404" {
            return Err(Error::NotFound);
        }

        let text = response.text().await?;
        let result = tokio::task::spawn_blocking(move || {
            let capture = REGEX
                .captures(&text)
                .and_then(|captures| captures.get(1))
                .ok_or(Error::MissingNuxtData)?;
            let obj: NuxtData<T> = serde_json::from_str(capture.as_str())?;
            Result::<NuxtData<T>, Error>::Ok(obj)
        })
        .await??;

        Ok(result)
    }

    /// Get the hentai info for a video url
    pub async fn get_hentai_video_info(&self, url: &str) -> Result<HentaiVideoInfo, Error> {
        self.get_nuxt(url).await
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
